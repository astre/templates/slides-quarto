# Quarto slides presentation

Template for producing [quarto slides](https://quarto.org/docs/presentations/) in HTML, PowerPoint or PDF.


## How to use

- Fork this project and make it independent:

    `Settings > General > Advanced > remove fork relationship`


- Edit `index.qmd` to make your slides in Markdown.

    See [quarto slides](https://quarto.org/docs/presentations/) for syntax and guidelines.

- Pushing to the repository will compile and publish the slides on line in GitLab pages, as a [Revealjs](https://revealjs.com/) web page.

    You can manually produce a PDF version of the slides using the integration presentation Tool `PDF Export Mode`. 

- Alternatively, you can directly compile the presentation into PDF using Beamer/LaTeX or in PowerPoint.

    You need to change `format: beamer` or `format: pptx` in the YAML header of `index.qmd`, and update `.gitlab-ci.yml` accordingly.

